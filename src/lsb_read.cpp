#include "image.h"
#include "mtrand.h"

int main(int argc, char** argv)
{
	if(argc!=3)
	{
		cout << argv[0] << " src.ppm dst.pgm\n"; 
		cout << "\t src.ppm  : image marquée\n"; 
		cout << "\t dst.pgm  : image lue\n\n"; 
		
		return -1; 
	}
	
	mtsrand(78425UL);
	
	image<rgb> 		src; 
	image<octet>	dst;
	
	src.read(argv[1]); 

	dst.resize(src.width(), src.height()); 
	
	
	
	// To do
	// boucle sur les pixels
	
	for(int y=0; y<src.height(); y++)
	{
		for(int x=0; x<src.width(); x++)
		{
			const int green = src(x, y).g & 1;
			if(mtrand()%2) {
				if(!green) {
					dst(x, y) = 0;
				} else {
					dst(x, y) = 255;
				}
			} else {
				if(green) {
					dst(x, y) = 0;
				} else {
					dst(x, y) = 255;
				}
			}
		}
	}
	
	
	dst.write(argv[2]); 
	
	return 1; 
}